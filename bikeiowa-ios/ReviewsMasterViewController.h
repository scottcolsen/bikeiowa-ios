//
//  ReviewsMasterViewController.h
//  bikeiowa-ios
//
//  Created by Scott Olsen on 1/19/13.
//  Copyright (c) 2013 Scott Olsen. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ReviewDataController;

@interface ReviewsMasterViewController : UIViewController <UITableViewDelegate,UITableViewDataSource >

@property (strong, nonatomic) ReviewDataController *dataController;
@property (strong, nonatomic) IBOutlet UITableView *reviewTable;
@property (strong, nonatomic) UIActivityIndicatorView *spinner;

@end

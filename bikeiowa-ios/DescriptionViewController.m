//
//  DescriptionViewController.m
//  BikeIowa
//
//  Created by Scott Olsen on 1/21/13.
//  Copyright (c) 2013 Scott Olsen. All rights reserved.
//

#import "DescriptionViewController.h"

@interface DescriptionViewController ()

@end

@implementation DescriptionViewController
@synthesize description, title, text;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    self.text.delegate = self;
    NSURL *baseURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] resourcePath]];
    NSMutableString *html = [NSMutableString stringWithString: @"<html><head><link rel=\"stylesheet\" type=\"text/css\" href=\"styles.css\"/><title></title></head><body style=\"background:transparent;\">"];
    [html appendString:description];
    [html appendString:@"</body></html>"];
    [text loadHTMLString:html baseURL:baseURL];

    self.navigationItem.title = title;
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL) webView:(UIWebView *)inWeb shouldStartLoadWithRequest:(NSURLRequest *)inRequest navigationType:(UIWebViewNavigationType)inType {
    if ( inType == UIWebViewNavigationTypeLinkClicked ) {
        [[UIApplication sharedApplication] openURL:[inRequest URL]];
        return NO;
    }
    
    return YES;
}

@end

//
//  FeatureAPI.m
//  BikeIowa
//
//  Created by Scott Olsen on 1/22/13.
//  Copyright (c) 2013 Scott Olsen. All rights reserved.
//

#import "FeatureAPI.h"
#import "Feature.h"

@implementation FeatureAPI
@synthesize apiFeatureList = _apiFeatureList, responseData = _responseData;

-(NSArray *) getFeatures
{
    NSMutableArray *featureList = [[NSMutableArray alloc] init];
    self.apiFeatureList = featureList;
    self.responseData = [NSMutableData data];
  //  NSString *apiUrl = @"http://localhost:3000/features.json";
       NSString *apiUrl = @"http://bikeiowa-api.heroku.com/features.json";
    
    NSMutableURLRequest *request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:apiUrl]];
    
    NSURLResponse* response = nil;
    NSData* data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:nil ];
    
    NSError *e = nil;
    NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData: data options: NSJSONReadingMutableContainers error: &e];
    
    if (!jsonArray) {
        NSLog(@"Error parsing JSON: %@", e);
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Error"
                                                        message:@"Server is down or you have no data connection. Please try again later."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    } else {
        
        for(NSDictionary *item in jsonArray) {
            
            // NSLog(@"Item: %@", item);
            
            [self addFeatureWithHeadline:[item objectForKey:@"Headline"] body:[item objectForKey:@"body"] url:[item objectForKey:@"url"] showDate:[item objectForKey:@"showDate"] externalAuthor:[item objectForKey:@"externalAuthor"] createdBy:[item objectForKey:@"createdBy"] source:[item objectForKey:@"source"]];
            
        }
    }
    
    return self.apiFeatureList;
}

-(void) setApiFeatureList:(NSMutableArray *)newList
{
    if(_apiFeatureList != newList){
        _apiFeatureList = [newList mutableCopy];
    }
}

-(void)addFeatureWithHeadline:(NSString *)headline body:(NSString *)body url:(NSString *)url showDate:(NSString *)showDate externalAuthor:(NSString *)externalAuthor createdBy:(NSString *)createdBy source:(NSString *)source
{
    Feature *feature;
    feature = [[Feature alloc] initWithHeadline:headline body:body url:url showDate:showDate externalAuthor:externalAuthor createdBy:createdBy source:source];
    [self.apiFeatureList addObject:feature];
}
@end

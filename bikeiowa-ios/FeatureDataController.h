//
//  FeatureDataController.h
//  BikeIowa
//
//  Created by Scott Olsen on 1/22/13.
//  Copyright (c) 2013 Scott Olsen. All rights reserved.
//

#import <Foundation/Foundation.h>
@class Feature;

@interface FeatureDataController : NSObject
@property (nonatomic, copy) NSMutableArray *masterFeatureList;

- (NSUInteger)countOfList;

- (Feature *)objectinListAtIndex:(NSUInteger)theIndex;
@end

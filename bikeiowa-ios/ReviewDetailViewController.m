//
//  ReviewDetailViewController.m
//  BikeIowa
//
//  Created by Scott Olsen on 1/22/13.
//  Copyright (c) 2013 Scott Olsen. All rights reserved.
//

#import "ReviewDetailViewController.h"
#import "Review.h"

@interface ReviewDetailViewController ()

@end

@implementation ReviewDetailViewController
@synthesize review = _review, headline = _headline, body = _body, date = _date, createdBy = _createdBy, externalAuthor = _externalAuthor;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    self.body.delegate = self;
    [super viewDidLoad];
	// Do any additional setup after loading the view.
      [self configureView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Managing the detail item

- (void)setReview:(Review *)newReview
{
    if (_review != newReview){
        _review = newReview;
        [self configureView];
    }
}

- (void)configureView
{
    // Update the user interface for the detail item.
    Review *theReview = self.review;
    
    if (theReview) {
        NSURL *baseURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] resourcePath]];
        NSMutableString *html = [NSMutableString stringWithString: @"<html><head><link rel=\"stylesheet\" type=\"text/css\" href=\"styles.css\"/><title></title></head><body style=\"background:transparent;\">"];
        [html appendString:theReview.body];
        [html appendString:@"</body></html>"];
        [self.body loadHTMLString:html baseURL:baseURL];
        
        self.navigationItem.title = theReview.headline;
        self.headline.text = theReview.headline;
        self.date.text = theReview.showDate;
        self.externalAuthor.text = [NSString stringWithFormat:@"Author: %@", theReview.externalAuthor];
        self.createdBy.text = [NSString stringWithFormat:@"Created by: %@", theReview.createdBy];
    }
}

-(BOOL) webView:(UIWebView *)inWeb shouldStartLoadWithRequest:(NSURLRequest *)inRequest navigationType:(UIWebViewNavigationType)inType {
    if ( inType == UIWebViewNavigationTypeLinkClicked ) {
        [[UIApplication sharedApplication] openURL:[inRequest URL]];
        return NO;
    }
    
    return YES;
}
- (IBAction)openReview:(id)sender {
       [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@", self.review.url ]]];
}
@end

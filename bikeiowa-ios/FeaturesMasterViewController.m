//
//  FeaturesMasterViewController.m
//  bikeiowa-ios
//
//  Created by Scott Olsen on 1/19/13.
//  Copyright (c) 2013 Scott Olsen. All rights reserved.
//

#import "FeaturesMasterViewController.h"
#import "Feature.h"
#import "FeatureDataController.h"
#import "FeatureDetailViewController.h"

@interface FeaturesMasterViewController ()

@end

@implementation FeaturesMasterViewController
@synthesize dataController = _dataController, featureTable = _featureTable, spinner = _spinner;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    UIBarButtonItem *button = [[UIBarButtonItem alloc]
                               initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh
                               target:self
                               action:@selector(refresh:)];
    self.navigationItem.rightBarButtonItem = button;
    
    [self refresh:nil];
    [super viewDidLoad];
    [self.featureTable reloadData];
	// Do any additional setup after loading the view.
}

-(IBAction)refresh:(id)sender
{
    _spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    _spinner.frame = CGRectMake(150,200, 20, 20);
    _spinner.color = [UIColor colorWithRed:89.0/255.0 green:199.0/255.0 blue:5.0/255.0 alpha:1];
     _spinner.center = self.view.center;
    [self.view addSubview:_spinner];
    
    [_spinner startAnimating];
    
    [self performSelector:@selector(hitApi) withObject:nil afterDelay:0.01];
    
}

-(void)hitApi{
    FeatureDataController *aDataController = [[FeatureDataController alloc] init];
    _dataController = aDataController;
    [self.featureTable reloadData];
    [_spinner stopAnimating];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated{
    [self.featureTable reloadData];
    
    [super viewDidAppear:animated];
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.dataController countOfList];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"FeatureCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    UIView* bgview = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, 1)];
    bgview.opaque = YES;
    bgview.backgroundColor = [UIColor whiteColor];
    [cell setBackgroundView:bgview];
    
    Feature *featureAtIndex = [self.dataController objectinListAtIndex:indexPath.row];
    
    UILabel *topLabel;
    topLabel = (UILabel *)[cell viewWithTag:1];
    topLabel.text = featureAtIndex.headline;
    
    UILabel *bottomLabel;
    bottomLabel = (UILabel *)[cell viewWithTag:2];
    bottomLabel.text = [NSString stringWithFormat:@"%@ - %@",  featureAtIndex.showDate,featureAtIndex.externalAuthor];
    
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return NO;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    FeatureDetailViewController *detailViewController = [segue destinationViewController] ;
    detailViewController.feature = [self.dataController objectinListAtIndex:[self.featureTable indexPathForSelectedRow].row];
}


@end

//
//  AppDelegate.h
//  bikeiowa-ios
//
//  Created by Scott Olsen on 1/19/13.
//  Copyright (c) 2013 Scott Olsen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

//
//  EventDataController.h
//  bikeiowa-ios
//
//  Created by Scott Olsen on 1/19/13.
//  Copyright (c) 2013 Scott Olsen. All rights reserved.
//

#import <Foundation/Foundation.h>
@class Event;
@interface EventDataController : NSObject

@property (nonatomic, copy) NSMutableArray *masterEventList;

- (NSUInteger)countOfList;

- (Event *)objectinListAtIndex:(NSUInteger)theIndex;


@end

//
//  NewsAPI.m
//  BikeIowa
//
//  Created by Scott Olsen on 1/22/13.
//  Copyright (c) 2013 Scott Olsen. All rights reserved.
//

#import "NewsAPI.h"
#import "News.h"

@implementation NewsAPI
@synthesize apiNewsList = _apiNewsList, responseData = _responseData;

-(NSArray *) getNews
{
    
    NSMutableArray *newsList = [[NSMutableArray alloc] init];
    self.apiNewsList = newsList;
    self.responseData = [NSMutableData data];
//NSString *apiUrl = @"http://localhost:3000/news.json";
       NSString *apiUrl = @"http://bikeiowa-api.heroku.com/news.json";

    NSMutableURLRequest *request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:apiUrl]];
    
    NSURLResponse* response = nil;
    NSData* data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:nil ];
    
    NSError *e = nil;
    NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData: data options: NSJSONReadingMutableContainers error: &e];
    
    if (!jsonArray) {
        NSLog(@"Error parsing JSON: %@", e);
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Error"
                                                        message:@"Server is down or you have no data connection. Please try again later."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    } else {
        
        for(NSDictionary *item in jsonArray) {
            
            // NSLog(@"Item: %@", item);
            
            [self addNewsWithHeadline:[item objectForKey:@"Headline"] body:[item objectForKey:@"body"] url:[item objectForKey:@"url"] newsDate:[item objectForKey:@"newsDate"] externalAuthor:[item objectForKey:@"externalAuthor"] createdBy:[item objectForKey:@"createdBy"] source:[item objectForKey:@"source"]];
                           
        }
    }

    return self.apiNewsList;
}

-(void) setApiNewsList:(NSMutableArray *)newList
{
    if(_apiNewsList != newList){
        _apiNewsList = [newList mutableCopy];
    }
}

-(void)addNewsWithHeadline:(NSString *)headline body:(NSString *)body url:(NSString *)url newsDate:(NSString *)newsDate externalAuthor:(NSString *)externalAuthor createdBy:(NSString *)createdBy source:(NSString *)source
{
    News *news;
    news = [[News alloc] initWithHeadline:headline body:body url:url newsDate:newsDate externalAuthor:externalAuthor createdBy:createdBy source:source];
    [self.apiNewsList addObject:news];
}
@end

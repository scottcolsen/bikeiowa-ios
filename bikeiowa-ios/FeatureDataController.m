//
//  FeatureDataController.m
//  BikeIowa
//
//  Created by Scott Olsen on 1/22/13.
//  Copyright (c) 2013 Scott Olsen. All rights reserved.
//

#import "FeatureDataController.h"
#import "FeatureAPI.h"
#import "Feature.h"

@interface FeatureDataController()
- (void)initializeDefaultDataList;

@end

@implementation FeatureDataController
@synthesize masterFeatureList = _masterFeatureList;

-(void)initializeDefaultDataList{
    self.masterFeatureList = [[FeatureAPI alloc] getFeatures];
}

-(void)setMasterFeatureList:(NSMutableArray *)masterFeatureList{
    if (_masterFeatureList != masterFeatureList){
        _masterFeatureList = [masterFeatureList mutableCopy];
    }
}

- (id)init{
    if (self = [super init]){
        [self initializeDefaultDataList];
        return self;
    }
    return nil;
}

- (NSUInteger)countOfList{
    return [self.masterFeatureList count];
}

- (Feature *)objectinListAtIndex:(NSUInteger)theIndex{
    return [self.masterFeatureList objectAtIndex:theIndex];
}


@end

//
//  EventsMasterViewController.h
//  bikeiowa-ios
//
//  Created by Scott Olsen on 1/19/13.
//  Copyright (c) 2013 Scott Olsen. All rights reserved.
//

#import <UIKit/UIKit.h>

@class EventDataController;

@interface EventsMasterViewController : UIViewController <UITableViewDelegate,UITableViewDataSource >

@property (strong, nonatomic) EventDataController *dataController;
@property (weak, nonatomic) IBOutlet UITableView *eventTable;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *previousMonth;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *nextMonth;
- (IBAction)previousMonthClick:(id)sender;
- (IBAction)nextMonthClick:(id)sender;
@property (strong, nonatomic) UIActivityIndicatorView *spinner;


@end

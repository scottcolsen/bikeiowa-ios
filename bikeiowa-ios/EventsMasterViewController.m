//
//  EventsMasterViewController.m
//  bikeiowa-ios
//
//  Created by Scott Olsen on 1/19/13.
//  Copyright (c) 2013 Scott Olsen. All rights reserved.
//

#import "EventsMasterViewController.h"
#import "EventsDetailViewController.h"
#import "EventDataController.h"
#import "Event.h"

@interface EventsMasterViewController ()

@end

@implementation EventsMasterViewController
@synthesize dataController = _dataController, eventTable = _eventTable, previousMonth = _previousMonth, nextMonth = _nextMonth, spinner = _spinner;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [self refresh:nil];
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
}


-(IBAction)refresh:(id)sender
{
    _spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    _spinner.frame = CGRectMake(150,200, 20, 20);
    _spinner.center = self.view.center;
    _spinner.color = [UIColor colorWithRed:89.0/255.0 green:199.0/255.0 blue:5.0/255.0 alpha:1];
    
    [self.view addSubview:_spinner];
    
    [_spinner startAnimating];
    
    [self performSelector:@selector(hitApi) withObject:nil afterDelay:0.01];
    
}

-(void)hitApi{
    EventDataController *aDataController = [[EventDataController alloc] init];
    _dataController = aDataController;
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *nextMonthName = [prefs stringForKey:@"NextMonthName"];
    NSString *previousMonthName = [prefs stringForKey:@"PreviousMonthName"];
    NSString *fullMonthName = [prefs stringForKey:@"FullMonthName"];
    [_previousMonth setTitle:previousMonthName];
    [_nextMonth setTitle:nextMonthName];
    self.navigationItem.title = [fullMonthName stringByAppendingString:@" Events"];
    [self.eventTable reloadData];
    [_spinner stopAnimating];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{    
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated{
    [self.eventTable reloadData];
    
    [super viewDidAppear:animated];
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.dataController countOfList];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"EventCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    UIView* bgview = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, 1)];
    bgview.opaque = YES;
    bgview.backgroundColor = [UIColor whiteColor];
    [cell setBackgroundView:bgview];
    
    Event *eventAtIndex = [self.dataController objectinListAtIndex:indexPath.row];
    
    UILabel *topLabel;
    topLabel = (UILabel *)[cell viewWithTag:1];
    topLabel.text = eventAtIndex.rideName;
    
    UILabel *bottomLabel;
    bottomLabel = (UILabel *)[cell viewWithTag:2];
    bottomLabel.text = [NSString stringWithFormat:@"%@, %@ - %@", eventAtIndex.city, eventAtIndex.state, eventAtIndex.eventType];
    
    UILabel *dateLabel;
    dateLabel = (UILabel *)[cell viewWithTag:3];
    dateLabel.text = eventAtIndex.startDate;
    
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return NO;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    EventsDetailViewController *detailViewController = [segue destinationViewController] ;
    detailViewController.event = [self.dataController objectinListAtIndex:[self.eventTable indexPathForSelectedRow].row];
}


- (IBAction)previousMonthClick:(id)sender {
    _dataController = NULL;
    [self.eventTable reloadData];
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *previousMonth = [prefs objectForKey:@"PreviousMonthDate"];
    [prefs setObject:previousMonth forKey:@"StartDate"];
    
     [self refresh:nil];
}

- (IBAction)nextMonthClick:(id)sender {
    _dataController = NULL;
    [self.eventTable reloadData];
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *nextMonth = [prefs objectForKey:@"NextMonthDate"];
    [prefs setObject:nextMonth forKey:@"StartDate"];

    [self refresh:nil];
}

@end

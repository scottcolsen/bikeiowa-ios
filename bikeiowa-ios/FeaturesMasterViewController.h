//
//  FeaturesMasterViewController.h
//  bikeiowa-ios
//
//  Created by Scott Olsen on 1/19/13.
//  Copyright (c) 2013 Scott Olsen. All rights reserved.
//

#import <UIKit/UIKit.h>
@class FeatureDataController;

@interface FeaturesMasterViewController : UIViewController <UITableViewDelegate,UITableViewDataSource >

@property (strong, nonatomic) FeatureDataController *dataController;
@property (strong, nonatomic) IBOutlet UITableView *featureTable;
@property (strong, nonatomic) UIActivityIndicatorView *spinner;
@end

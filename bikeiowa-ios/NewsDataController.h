//
//  NewsDataController.h
//  BikeIowa
//
//  Created by Scott Olsen on 1/22/13.
//  Copyright (c) 2013 Scott Olsen. All rights reserved.
//

#import <Foundation/Foundation.h>
@class News;
@interface NewsDataController : NSObject

@property (nonatomic, copy) NSMutableArray *masterNewsList;

- (NSUInteger)countOfList;

- (News *)objectinListAtIndex:(NSUInteger)theIndex;

@end

//
//  EventDataController.m
//  bikeiowa-ios
//
//  Created by Scott Olsen on 1/19/13.
//  Copyright (c) 2013 Scott Olsen. All rights reserved.
//

#import "EventDataController.h"
#import "EventsAPI.h"
#import "Event.h"
#import "AppDelegate.h"


@interface EventDataController ()
- (void)initializeDefaultDataList;
@end

@implementation EventDataController
@synthesize masterEventList = _masterEventList;


- (void)initializeDefaultDataList {
    self.masterEventList = [[EventsAPI alloc] getEvents];
}


- (void)setMasterEventList:(NSMutableArray *)newList{
    if(_masterEventList != newList){
        _masterEventList = [newList mutableCopy];
    }
}

- (id)init{
    if (self = [super init]){
        [self initializeDefaultDataList];
        return self;
    }
    return nil;
}

- (NSUInteger)countOfList{
    return [self.masterEventList count];
}

- (Event *)objectinListAtIndex:(NSUInteger)theIndex{
    return [self.masterEventList objectAtIndex:theIndex];
}


@end

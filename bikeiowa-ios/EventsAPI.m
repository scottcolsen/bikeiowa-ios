//
//  EventsAPI.m
//  bikeiowa-ios
//
//  Created by Scott Olsen on 1/19/13.
//  Copyright (c) 2013 Scott Olsen. All rights reserved.
//

#import "EventsAPI.h"
#import "Event.h"
#import "AppDelegate.h"

@implementation EventsAPI
@synthesize apiEventList = _apiEventList, responseData = _responseData;

-(NSArray *) getEvents
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *startDate = [prefs objectForKey:@"StartDate"];
    
    NSMutableArray *eventList = [[NSMutableArray alloc] init];
    self.apiEventList = eventList;
    self.responseData = [NSMutableData data];
    //NSString *apiUrl = @"http://localhost:3000/events.json";
    NSString *apiUrl = @"http://bikeiowa-api.heroku.com/events.json";
    if (startDate != NULL){
        apiUrl = [apiUrl stringByAppendingString:@"?start_date="];
        apiUrl = [apiUrl stringByAppendingString:startDate];
    }
    NSMutableURLRequest *request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:apiUrl]];
    
    NSURLResponse* response = nil;
    NSData* data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:nil ];
    
    NSError *e = nil;
    NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData: data options: NSJSONReadingMutableContainers error: &e];
    
    
    if (!jsonArray) {
        NSLog(@"Error parsing JSON: %@", e);
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Error"
                                                        message:@"Server is down or you have no data connection. Please try again later."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    } else {
        
        for(NSDictionary *item in jsonArray) {
            
            // NSLog(@"Item: %@", item);
            
            if([jsonArray indexOfObject:item] == 0 ){
                
                [prefs setObject:[item objectForKey:@"NextMonthName"] forKey:@"NextMonthName"];
                [prefs setObject:[item objectForKey:@"PreviousMonthName"] forKey:@"PreviousMonthName"];
                [prefs setObject:[item objectForKey:@"PreviousMonthDate"] forKey:@"PreviousMonthDate"];
                [prefs setObject:[item objectForKey:@"NextMonthDate"] forKey:@"NextMonthDate"];
                [prefs setObject:[item objectForKey:@"FullMonthName"] forKey:@"FullMonthName"];
            }
            else{
                //                NSLog(@"%@", [item objectForKey:@"RideDesc"]);
                [self addEventWithRideName:[item objectForKey:@"RideName"] startDate:[item objectForKey:@"StartDate"]startTime:[item objectForKey:@"StartTime"] rideLocation:[item objectForKey:@"RideLocation"] rideDesc:[item objectForKey:@"RideDesc"] city:[item objectForKey:@"City"] state:[item objectForKey:@"State"]zip:[item objectForKey:@"Zip"] eventType:[item objectForKey:@"eventtype"] rideRoute:[item objectForKey:@"RideRoute"] registration:[item objectForKey:@"Registration"]  costs:[item objectForKey:@"Costs"]  distance:[item objectForKey:@"Distance"]  rideStartAddress:[item objectForKey:@"RideStartAddress"]  contactPerson:[item objectForKey:@"ContactPerson"]  email1:[item objectForKey:@"Email1"]  contactPhone:[item objectForKey:@"ContactPhone"]  numberRiders:[item objectForKey:@"noriders"]  terrain:[item objectForKey:@"terrain"]  riderType:[item objectForKey:@"ridertype"] host:[item objectForKey:@"Host"] rideId:[item objectForKey:@"RideID"]];
            }
        }
    }
    [prefs setObject:nil forKey:@"StartDate"];
    return self.apiEventList;
}

- (void)setApiEventList:(NSMutableArray *)newList{
    if(_apiEventList != newList){
        _apiEventList = [newList mutableCopy];
    }
}

-(void)addEventWithRideName:(NSString *)rideName startDate:(NSString *)startDate startTime:(NSString *)startTime rideLocation:(NSString *)rideLocation rideDesc:(NSString *)rideDesc city:(NSString *)city state:(NSString *)state zip:(NSString *)zip eventType:(NSString *)eventType rideRoute:(NSString *)rideRoute registration:(NSString *)registration costs:(NSString *)costs distance:(NSString *)distance rideStartAddress:(NSString *)rideStartAddress contactPerson:(NSString *)contactPerson email1:(NSString *)email1 contactPhone:(NSString *)contactPhone numberRiders:(NSString *)numberRiders terrain:(NSString *)terrain riderType:(NSString *)riderType host:(NSString *)host rideId:(NSString *)rideId{
    Event *event;
    event = [[Event alloc] initWithRideName:rideName startDate:startDate startTime:startTime rideLocation:rideLocation rideDesc:rideDesc city:city state:state zip:zip eventType:eventType rideRoute:rideRoute registration:registration costs:costs distance:distance rideStartAddress:rideStartAddress contactPerson:contactPerson email1:email1 contactPhone:contactPhone numberRiders:numberRiders terrain:terrain riderType:riderType host:host rideId:rideId];
    [self.apiEventList addObject:event];
}

@end

//
//  News.m
//  BikeIowa
//
//  Created by Scott Olsen on 1/22/13.
//  Copyright (c) 2013 Scott Olsen. All rights reserved.
//

#import "News.h"

@implementation News
@synthesize headline = _headline, body = _body, url = _url, newsDate = _newsDate, externalAuthor = _externalAuthor, createdBy = _createdBy, source = _source;

-(id)initWithHeadline:(NSString *)headline body:(NSString *)body url:(NSString *)url newsDate:(NSString *)newsDate externalAuthor:(NSString *)externalAuthor createdBy:(NSString *)createdBy source:(NSString *)source
{
    self = [super init];
    if(self)
    {
        _headline = headline;
        _body = body;
        _url = url;
        _newsDate = newsDate;
        _externalAuthor = externalAuthor;
        _createdBy = createdBy;
        _source = source;
        
        return self;
    }
    return nil;
}
@end

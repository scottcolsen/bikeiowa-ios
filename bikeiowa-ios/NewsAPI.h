//
//  NewsAPI.h
//  BikeIowa
//
//  Created by Scott Olsen on 1/22/13.
//  Copyright (c) 2013 Scott Olsen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NewsAPI : NSObject

@property (nonatomic, copy) NSMutableArray *apiNewsList;
@property (nonatomic, strong) NSMutableData *responseData;

-(NSMutableArray *) getNews;

-(void)addNewsWithHeadline:(NSString *)headline body:(NSString *)body url:(NSString *)url newsDate:(NSString *)newsDate externalAuthor:(NSString *)externalAuthor createdBy:(NSString *)createdBy source:(NSString *)source;
@end

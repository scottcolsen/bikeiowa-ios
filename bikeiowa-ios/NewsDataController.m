//
//  NewsDataController.m
//  BikeIowa
//
//  Created by Scott Olsen on 1/22/13.
//  Copyright (c) 2013 Scott Olsen. All rights reserved.
//

#import "NewsDataController.h"
#import "NewsAPI.h"
#import "News.h"

@interface NewsDataController()
- (void)initializeDefaultDataList;

@end

@implementation NewsDataController
@synthesize masterNewsList = _masterNewsList;


-(void)initializeDefaultDataList{
    self.masterNewsList = [[NewsAPI alloc] getNews];
}

-(void)setMasterNewsList:(NSMutableArray *)masterNewsList{
    if (_masterNewsList != masterNewsList){
        _masterNewsList = [masterNewsList mutableCopy];
    }
}

- (id)init{
    if (self = [super init]){
        [self initializeDefaultDataList];
        return self;
    }
    return nil;
}

- (NSUInteger)countOfList{
    return [self.masterNewsList count];
}

- (News *)objectinListAtIndex:(NSUInteger)theIndex{
    return [self.masterNewsList objectAtIndex:theIndex];
}

@end

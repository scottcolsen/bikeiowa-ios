//
//  NewsMasterViewController.h
//  bikeiowa-ios
//
//  Created by Scott Olsen on 1/19/13.
//  Copyright (c) 2013 Scott Olsen. All rights reserved.
//

#import <UIKit/UIKit.h>
@class NewsDataController;

@interface NewsMasterViewController : UIViewController <UITableViewDelegate,UITableViewDataSource >

@property (strong, nonatomic) NewsDataController *dataController;
@property (weak, nonatomic) IBOutlet UITableView *newsTable;
@property (strong, nonatomic) UIActivityIndicatorView *spinner;

@end

//
//  Review.m
//  BikeIowa
//
//  Created by Scott Olsen on 1/22/13.
//  Copyright (c) 2013 Scott Olsen. All rights reserved.
//

#import "Review.h"

@implementation Review
@synthesize headline = _headline, body = _body, url = _url, showDate = _showDate, externalAuthor = _externalAuthor, createdBy = _createdBy, source = _source;

-(id)initWithHeadline:(NSString *)headline body:(NSString *)body url:(NSString *)url showDate:(NSString *)showDate externalAuthor:(NSString *)externalAuthor createdBy:(NSString *)createdBy source:(NSString *)source
{
    self = [super init];
    if(self)
    {
        _headline = headline;
        _body = body;
        _url = url;
        _showDate = showDate;
        _externalAuthor = externalAuthor;
        _createdBy = createdBy;
        _source = source;
        
        return self;
    }
    return nil;
}
@end

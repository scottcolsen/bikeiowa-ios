//
//  EventsAPI.h
//  bikeiowa-ios
//
//  Created by Scott Olsen on 1/19/13.
//  Copyright (c) 2013 Scott Olsen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EventsAPI : NSObject

@property (nonatomic, copy) NSMutableArray *apiEventList;
@property (nonatomic, strong) NSMutableData *responseData;

-(NSMutableArray *) getEvents;

-(void)addEventWithRideName:(NSString *)rideName startDate:(NSString *)startDate startTime:(NSString *)startTime rideLocation:(NSString *)rideLocation rideDesc:(NSString *)rideDesc city:(NSString *)city state:(NSString *)state zip:(NSString *)zip eventType:(NSString *)eventType rideRoute:(NSString *)rideRoute registration:(NSString *)registration costs:(NSString *)costs distance:(NSString *)distance rideStartAddress:(NSString *)rideStartAddress contactPerson:(NSString *)contactPerson email1:(NSString *)email1 contactPhone:(NSString *)contactPhone numberRiders:(NSString *)numberRiders terrain:(NSString *)terrain riderType:(NSString *)riderType host:(NSString *)host rideId:(NSString *)rideId;
@end

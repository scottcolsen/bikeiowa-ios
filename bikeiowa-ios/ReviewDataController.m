//
//  ReviewDataController.m
//  BikeIowa
//
//  Created by Scott Olsen on 1/22/13.
//  Copyright (c) 2013 Scott Olsen. All rights reserved.
//

#import "ReviewDataController.h"
#import "Review.h"
#import "ReviewAPI.h"

@interface ReviewDataController()
- (void)initializeDefaultDataList;

@end


@implementation ReviewDataController
@synthesize masterReviewList = _masterReviewList;

-(void)initializeDefaultDataList{
    self.masterReviewList = [[ReviewAPI alloc] getReviews];
}

-(void)setMasterReviewList:(NSMutableArray *)masterReviewList{
    if(_masterReviewList != masterReviewList){
        _masterReviewList = [masterReviewList mutableCopy];
    }
}

- (id)init{
    if (self = [super init]){
        [self initializeDefaultDataList];
        return self;
    }
    return nil;
}

- (NSUInteger)countOfList{
    return [self.masterReviewList count];
}

- (Review *)objectinListAtIndex:(NSUInteger)theIndex{
    return [self.masterReviewList objectAtIndex:theIndex];
}



@end

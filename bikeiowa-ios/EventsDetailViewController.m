//
//  EventsDetailViewController.m
//  bikeiowa-ios
//
//  Created by Scott Olsen on 1/19/13.
//  Copyright (c) 2013 Scott Olsen. All rights reserved.
//

#import "EventsDetailViewController.h"
#import "Event.h"
#import "DescriptionViewController.h"
#import <QuartzCore/QuartzCore.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import "NSString_stripHtml.h"

@interface EventsDetailViewController ()

@end

@implementation EventsDetailViewController
@synthesize event = _event, rideName, phone, address, cityStateZip, date, riders, registrationButton, routeButton, descriptionButton, host, eventType, distance, terrain, riderType, contactName, email, routeTextView, registrationTextView, descriptionTextView, description, route, registration, descriptionWebView,registrationWebView, routeWebView,
mainScrollView;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark - Managing the detail item

- (void)setEvent:(Event *) newEvent
{
    if (_event != newEvent){
        _event = newEvent;
        [self configureView];
    }
}

- (void)configureView
{
    // Update the user interface for the detail item.
    Event *theEvent = self.event;
    
    if (theEvent) {
        NSURL *baseURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] resourcePath]];
        NSMutableString *html = [NSMutableString stringWithString: @"<html><head><link rel=\"stylesheet\" type=\"text/css\" href=\"styles.css\"/><title></title></head><body style=\"background:transparent;\">"];
        [html appendString:theEvent.getDescription];
        [html appendString:@"</body></html>"];
        [descriptionWebView loadHTMLString:html baseURL:baseURL];
        
        html = [NSMutableString stringWithString: @"<html><head><link rel=\"stylesheet\" type=\"text/css\" href=\"styles.css\"/><title></title></head><body style=\"background:transparent;\">"];
        [html appendString:theEvent.getRegistration];
        [html appendString:@"</body></html>"];
        [registrationWebView loadHTMLString:html baseURL:baseURL];
        
        html = [NSMutableString stringWithString: @"<html><head><link rel=\"stylesheet\" type=\"text/css\" href=\"styles.css\"/><title></title></head><body style=\"background:transparent;\">"];
        [html appendString:theEvent.getRoute];
        [html appendString:@"</body></html>"];
        [routeWebView loadHTMLString:html baseURL:baseURL];
        
        
        self.navigationItem.title = theEvent.rideName;
        rideName.text = theEvent.rideName;
        date.text = [NSString stringWithFormat:@"%@ - %@",theEvent.startDate, theEvent.startTime];
        host.text = theEvent.host;
        riders.text = [NSString stringWithFormat:@"%@ riders expected",theEvent.numberRiders];
        phone.text = theEvent.contactPhone;
        address.text = theEvent.rideStartAddress;
        eventType.text = [NSString stringWithFormat:@"Event Type: %@", theEvent.eventType];
        cityStateZip.text = [NSString stringWithFormat:@"%@, %@ %@", theEvent.city, theEvent.state, theEvent.zip];
        distance.text = theEvent.distance;
        terrain.text = theEvent.terrain;
        riderType.text = theEvent.riderType;
        contactName.text = theEvent.contactPerson;
        email.text = theEvent.email1;
        description.text = [theEvent.getDescription stripHtml];
        registration.text = [theEvent.getRegistration stripHtml];
        route.text = [theEvent.getRoute stripHtml];
    }
}

- (void)viewDidLoad
{
    self.descriptionWebView.delegate = self;
    self.registrationWebView.delegate = self;
    self.routeWebView.delegate = self;
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    [self configureView];
}

- (void) viewDidAppear:(BOOL)animated
{
   mainScrollView.contentSize = CGSizeMake(mainScrollView.frame.size.width, mainScrollView.frame.size.height + 320);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"description"]) {
        DescriptionViewController *descriptionViewController = [segue destinationViewController];
        descriptionViewController.description = self.event.getDescription;
        descriptionViewController.title = self.event.rideName;
        
    }
    
    if ([[segue identifier] isEqualToString:@"Registration"]) {
        DescriptionViewController *descriptionViewController = [segue destinationViewController];
        descriptionViewController.description = self.event.getRegistration;
        descriptionViewController.title = self.event.rideName;
        
    }
    
    if ([[segue identifier] isEqualToString:@"Route"]) {
        DescriptionViewController *descriptionViewController = [segue destinationViewController];
        descriptionViewController.description = self.event.getRoute;
        descriptionViewController.title = self.event.rideName;
    }
}

- (IBAction) openMap:(id)sender
{
    Class mapItemClass = [MKMapItem class];
    if (mapItemClass && [mapItemClass respondsToSelector:@selector(openMapsWithItems:launchOptions:)])
    {
        CLGeocoder *geocoder = [[CLGeocoder alloc] init];
        [geocoder geocodeAddressString: [self getFullAddress]
                     completionHandler:^(NSArray *placemarks, NSError *error) {
                         
                         // Convert the CLPlacemark to an MKPlacemark
                         // Note: There's no error checking for a failed geocode
                         CLPlacemark *geocodedPlacemark = [placemarks objectAtIndex:0];
                         MKPlacemark *placemark = [[MKPlacemark alloc]
                                                   initWithCoordinate:geocodedPlacemark.location.coordinate
                                                   addressDictionary:geocodedPlacemark.addressDictionary];
                         
                         // Create a map item for the geocoded address to pass to Maps app
                         MKMapItem *mapItem = [[MKMapItem alloc] initWithPlacemark:placemark];
                         [mapItem setName:geocodedPlacemark.name];
                         
                         // Pass the current location and destination map items to the Maps app
                         // Set the direction mode in the launchOptions dictionary
                         [MKMapItem openMapsWithItems:@[mapItem] launchOptions:nil];
                         
                     }];
    } else {
        NSString *url = [NSString stringWithFormat: @"http://maps.google.com/maps?q=%@", [self getFullAddress]];
        url = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        [[UIApplication sharedApplication] openURL: [NSURL URLWithString: url]];
    }
}

- (IBAction)openEvent:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://www.bikeiowa.com/Event/%@", self.event.rideId ]]];
}

- (NSString*) getFullAddress {
    return [NSString stringWithFormat:@"%@, %@, %@ %@", self.event.rideStartAddress,
     self.event.city, self.event.state, self.event.zip];
}

-(BOOL) webView:(UIWebView *)inWeb shouldStartLoadWithRequest:(NSURLRequest *)inRequest navigationType:(UIWebViewNavigationType)inType {
    if ( inType == UIWebViewNavigationTypeLinkClicked ) {
        [[UIApplication sharedApplication] openURL:[inRequest URL]];
        return NO;
    }
    
    return YES;
}

@end

//
//  ReviewDetailViewController.h
//  BikeIowa
//
//  Created by Scott Olsen on 1/22/13.
//  Copyright (c) 2013 Scott Olsen. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Review;

@interface ReviewDetailViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *headline;
@property (weak, nonatomic) IBOutlet UILabel *date;
@property (weak, nonatomic) IBOutlet UILabel *externalAuthor;
@property (weak, nonatomic) IBOutlet UILabel *createdBy;
@property (weak, nonatomic) IBOutlet UIWebView *body;
- (IBAction)openReview:(id)sender;

@property (strong, nonatomic) Review *review;
@end

//
//  ReviewAPI.m
//  BikeIowa
//
//  Created by Scott Olsen on 1/22/13.
//  Copyright (c) 2013 Scott Olsen. All rights reserved.
//

#import "ReviewAPI.h"
#import "Review.h"

@implementation ReviewAPI
@synthesize apiReviewList = _apiReviewList, responseData = _responseData;

-(NSArray *) getReviews
{
    NSMutableArray *reviewList = [[NSMutableArray alloc] init];
    self.apiReviewList = reviewList;
    self.responseData = [NSMutableData data];
    //NSString *apiUrl = @"http://localhost:3000/reviews.json";
       NSString *apiUrl = @"http://bikeiowa-api.heroku.com/reviews.json";
    
    NSMutableURLRequest *request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:apiUrl]];
    
    NSURLResponse* response = nil;
    NSData* data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:nil ];
    
    NSError *e = nil;
    NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData: data options: NSJSONReadingMutableContainers error: &e];
    
    if (!jsonArray) {
        NSLog(@"Error parsing JSON: %@", e);
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Error"
                                                        message:@"Server is down or you have no data connection. Please try again later."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    } else {
        
        for(NSDictionary *item in jsonArray) {
            
            // NSLog(@"Item: %@", item);
            
            [self addReviewWithHeadline:[item objectForKey:@"Headline"] body:[item objectForKey:@"body"] url:[item objectForKey:@"url"] showDate:[item objectForKey:@"showDate"] externalAuthor:[item objectForKey:@"externalAuthor"] createdBy:[item objectForKey:@"createdBy"] source:[item objectForKey:@"source"]];
            
        }
    }
    
    return self.apiReviewList;
}

-(void) setApiReviewList:(NSMutableArray *)newList
{
    if(_apiReviewList != newList){
        _apiReviewList = [newList mutableCopy];
    }
}

-(void)addReviewWithHeadline:(NSString *)headline body:(NSString *)body url:(NSString *)url showDate:(NSString *)showDate externalAuthor:(NSString *)externalAuthor createdBy:(NSString *)createdBy source:(NSString *)source
{
    Review *review;
    review = [[Review alloc] initWithHeadline:headline body:body url:url showDate:showDate externalAuthor:externalAuthor createdBy:createdBy source:source];
    [self.apiReviewList addObject:review];
}
@end

//
//  ReviewDataController.h
//  BikeIowa
//
//  Created by Scott Olsen on 1/22/13.
//  Copyright (c) 2013 Scott Olsen. All rights reserved.
//

#import <Foundation/Foundation.h>
@class Review;

@interface ReviewDataController : NSObject

@property (nonatomic, copy) NSMutableArray *masterReviewList;

- (NSUInteger)countOfList;

- (Review *)objectinListAtIndex:(NSUInteger)theIndex;
@end

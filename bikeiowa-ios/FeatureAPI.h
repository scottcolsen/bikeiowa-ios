//
//  FeatureAPI.h
//  BikeIowa
//
//  Created by Scott Olsen on 1/22/13.
//  Copyright (c) 2013 Scott Olsen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FeatureAPI : NSObject

@property (nonatomic, copy) NSMutableArray *apiFeatureList;
@property (nonatomic, strong) NSMutableData *responseData;

-(NSMutableArray *) getFeatures;

-(void)addFeatureWithHeadline:(NSString *)headline body:(NSString *)body url:(NSString *)url showDate:(NSString *)showDate externalAuthor:(NSString *)externalAuthor createdBy:(NSString *)createdBy source:(NSString *)source;
@end

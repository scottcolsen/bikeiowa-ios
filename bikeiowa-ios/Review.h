//
//  Review.h
//  BikeIowa
//
//  Created by Scott Olsen on 1/22/13.
//  Copyright (c) 2013 Scott Olsen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Review : NSObject
@property (nonatomic, copy) NSString *headline;
@property (nonatomic, copy) NSString *body;
@property (nonatomic, copy) NSString *url;
@property (nonatomic, copy) NSString *showDate;
@property (nonatomic, copy) NSString *externalAuthor;
@property (nonatomic, copy) NSString *createdBy;
@property (nonatomic, copy) NSString *source;

-(id)initWithHeadline:(NSString *)headline body:(NSString *)body url:(NSString *)url showDate:(NSString *)showDate externalAuthor:(NSString *)externalAuthor createdBy:(NSString *)createdBy source:(NSString *)source;

@end

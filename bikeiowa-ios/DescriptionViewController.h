//
//  DescriptionViewController.h
//  BikeIowa
//
//  Created by Scott Olsen on 1/21/13.
//  Copyright (c) 2013 Scott Olsen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DescriptionViewController : UIViewController
//@property (weak, nonatomic) IBOutlet UITextView *text;
@property (weak, nonatomic) NSString *description;
@property (weak, nonatomic) NSString *pageTitle;
@property (weak, nonatomic) IBOutlet UIWebView *text;

@end
